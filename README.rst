About
=====

This repository contains a simulation of layered Network Coding.

Build
=====

To configure the examples run:
::
  python waf configure
  
This will configure the project and download all the dependencies needed.

After configure run the following command to build:
::
  python waf build
  
If you experience problems with either of the two previous steps you can 
check the Kodo manual's `Getting Started`_ section for trubleshooting.

.. _`Getting Started`:
   https://kodo.readthedocs.org/en/latest/getting_started.html

Run
===

After building you have a couple of different simulations, the one we used in 
the paper is the ``heterogeneous_decoding``. It runs a single layered encoder
which sends packets to the three different types of recivers:

- l1 receiver: Decodes only L1
- l1l2 receiver: Decodes L1 but uses L2 to help
- l2 receiver: Decodes both L1 and L2 

The simulator can be started by typing:
::
  ./build/linux/heterogeneous_decoding
  
You can run the simulator with the ``--help`` option to see which parameters 
are supported:
::
  ./build/linux/heterogeneous_decoding --help



