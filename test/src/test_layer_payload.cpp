#include <gtest/gtest.h>

#include <kodo/layer_payload.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t max_layer_index_size() const
                {
                    return m_max_layer_index_size;
                }

                uint32_t max_coefficient_vector_size() const
                {
                    return m_max_coefficient_size;
                }

                uint32_t max_symbol_size() const
                {
                    return m_max_symbol_size;
                }

                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;
                uint32_t m_max_layer_index_size;
                uint32_t m_max_coefficient_size;

            };

        public:

            uint32_t layer_index_size() const
            {
                return m_layer_index_size;
            }

            uint32_t coefficient_vector_size() const
            {
                return m_coefficient_size;
            }

            uint32_t symbol_size() const
            {
                return m_symbol_size;
            }

            uint32_t m_symbol_size;
            uint32_t m_layer_index_size;
            uint32_t m_coefficient_size;

        };

        class dummy_stack
            : public layer_payload<dummy_layer>
        { };

        class dummy_factory
        { };

    }
}

TEST(TestLayerPayload, api)
{
    uint32_t max_symbols = rand();
    uint32_t max_symbol_size = rand();

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);
    EXPECT_EQ(factory.m_max_symbols, max_symbols);
    EXPECT_EQ(factory.m_max_symbol_size, max_symbol_size);

    factory.m_max_layer_index_size = rand();
    factory.m_max_coefficient_size = rand();

    EXPECT_EQ(factory.max_payload_size(), factory.m_max_symbol_size +
              factory.m_max_layer_index_size + factory.m_max_coefficient_size);

    kodo::dummy_stack stack;

    stack.m_symbol_size = rand();
    stack.m_layer_index_size = rand();
    stack.m_coefficient_size = rand();

    EXPECT_EQ(stack.payload_size(), stack.m_symbol_size +
              stack.m_layer_index_size + stack.m_coefficient_size);

}

