#include <gtest/gtest.h>

#include <kodo/layer_index.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            class factory
            {
            public:

                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;
            };

        };

        class dummy_stack
            : public layer_index<dummy_layer>
        { };

    }
}

TEST(TestLayerIndex, api)
{
    uint32_t max_symbols = rand();
    uint32_t max_symbol_size = rand();

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);
    EXPECT_EQ(factory.m_max_symbols, max_symbols);
    EXPECT_EQ(factory.m_max_symbol_size, max_symbol_size);
    EXPECT_EQ(factory.max_layer_index_size(),
              sizeof(kodo::dummy_stack::layer_index_type));

    kodo::dummy_stack stack;
    EXPECT_EQ(stack.layer_index_size(),
              sizeof(kodo::dummy_stack::layer_index_type));

    EXPECT_EQ(factory.max_layer_index_size(),
              stack.layer_index_size());

}

