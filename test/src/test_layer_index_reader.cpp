#include <gtest/gtest.h>

#include <kodo/layer_index_reader.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            typedef uint32_t layer_index_type;

        };

        class dummy_stack
            : public layer_index_reader<dummy_layer>
        { };

    }
}

TEST(TestLayerIndexReader, api)
{
    typedef kodo::dummy_stack::layer_index_type layer_index_type;

    kodo::dummy_stack stack;

    std::vector<uint8_t> buffer(sizeof(layer_index_type));
    sak::big_endian::put<layer_index_type>(12U, &buffer[0]);

    stack.read_layer_index(&buffer[0]);
    EXPECT_EQ(stack.layer_index(), 12U);

}

