#include <gtest/gtest.h>

#include <kodo/layer_decoder_info.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            typedef uint32_t layer_index_type;

        public:

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t max_symbols() const
                {
                    return m_max_symbols;
                }

                const std::vector<layer_index_type>& layer_symbols() const
                {
                    return m_layer_symbols;
                }

            public:

                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;
                std::vector<layer_index_type> m_layer_symbols;
            };

        public:

            template<class Factory>
            void initialize(Factory &the_factory)
            {
                (void) the_factory;
            }

        };

        class dummy_stack
            : public layer_decoder_info<dummy_layer>
        { };

    }
}

TEST(TestLayerDecoderInfo, api)
{
    uint32_t max_symbols = 20U;
    uint32_t max_symbol_size = 100U;

    std::vector<uint32_t> layer_symbols;
    layer_symbols.push_back(6U);
    layer_symbols.push_back(7U);
    layer_symbols.push_back(8U);
    layer_symbols.push_back(9U);
    layer_symbols.push_back(10U);

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);
    factory.set_decode_layer_index(2);
    factory.set_use_layer_index(3);
    factory.m_layer_symbols = layer_symbols;

    EXPECT_EQ(factory.decode_layer_index(), 2U);
    EXPECT_EQ(factory.use_layer_index(), 3U);

    kodo::dummy_stack stack;
    stack.initialize(factory);

    EXPECT_EQ(stack.decode_layer_index(), 2U);
    EXPECT_EQ(stack.use_layer_index(), 3U);

}

