#include <gtest/gtest.h>

#include <kodo/layer_setup.hpp>


TEST(TestLayerSetup, default_layers)
{
    std::vector<uint32_t> layers = kodo::default_layers(10, 6, 2);
    EXPECT_EQ(layers.size(), 3U);
    EXPECT_EQ(layers[0], 10U);
    EXPECT_EQ(layers[1], 13U);
    EXPECT_EQ(layers[2], 16U);

    layers = kodo::default_layers(10, 6, 6);
    EXPECT_EQ(layers.size(), 7U);
    EXPECT_EQ(layers[0], 10U);
    EXPECT_EQ(layers[1], 11U);
    EXPECT_EQ(layers[2], 12U);
    EXPECT_EQ(layers[3], 13U);
    EXPECT_EQ(layers[4], 14U);
    EXPECT_EQ(layers[5], 15U);
    EXPECT_EQ(layers[6], 16U);

    layers = kodo::default_layers(16, 0, 0);
    EXPECT_EQ(layers.size(), 1U);
    EXPECT_EQ(layers[0], 16U);

    layers = kodo::default_layers(10, 6, 1);
    EXPECT_EQ(layers.size(), 2U);
    EXPECT_EQ(layers[0], 10U);
    EXPECT_EQ(layers[1], 16U);

}


TEST(TestLayerSetup, default_probabilities)
{
    std::vector<double> layers = kodo::default_probabilities(0.6, 2, 3);
    EXPECT_EQ(layers.size(), 3U);
    EXPECT_DOUBLE_EQ(layers[0], 0.6);
    EXPECT_DOUBLE_EQ(layers[1], 0.2);
    EXPECT_DOUBLE_EQ(layers[2], 0.2);

    layers = kodo::default_probabilities(0.4, 6, 7);
    EXPECT_EQ(layers.size(), 7U);
    EXPECT_DOUBLE_EQ(layers[0], 0.4);
    EXPECT_DOUBLE_EQ(layers[1], 0.1);
    EXPECT_DOUBLE_EQ(layers[2], 0.1);
    EXPECT_DOUBLE_EQ(layers[3], 0.1);
    EXPECT_DOUBLE_EQ(layers[4], 0.1);
    EXPECT_DOUBLE_EQ(layers[5], 0.1);
    EXPECT_DOUBLE_EQ(layers[6], 0.1);

    layers = kodo::default_probabilities(1.0, 0, 1);
    EXPECT_EQ(layers.size(), 1U);
    EXPECT_DOUBLE_EQ(layers[0], 1.0);

    layers = kodo::default_probabilities(0.6, 1, 2);
    EXPECT_EQ(layers.size(), 2U);
    EXPECT_DOUBLE_EQ(layers[0], 0.6);
    EXPECT_DOUBLE_EQ(layers[1], 0.4);

}

