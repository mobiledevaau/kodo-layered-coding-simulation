#include <gtest/gtest.h>

#include <kodo/layer_payload_encoder.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            void select_layer()
            {
                m_layer_selected = true;
            }

            void encode_symbol(uint8_t *symbol_data, uint8_t *coefficients)
            {
                m_encode_symbol_data = symbol_data;
                m_encode_coefficients = coefficients;
            }

            void write_layer_index(uint8_t* layer_index)
            {
                m_write_layer_index = layer_index;
            }

            void write_coefficients(uint8_t* coefficients)
            {
                m_write_coefficients = coefficients;
            }

            uint32_t layer_index_size() const
            {
                return m_layer_index_size;
            }

            uint32_t coefficient_vector_size() const
            {
                return m_coefficients_size;
            }

            uint32_t payload_size() const
            {
                return m_payload_size;
            }

            bool m_layer_selected;

            uint32_t m_layer_index_size;
            uint32_t m_coefficients_size;
            uint32_t m_payload_size;

            uint8_t* m_encode_symbol_data;
            uint8_t* m_encode_coefficients;

            uint8_t* m_write_layer_index;
            uint8_t* m_write_coefficients;

        };

        class dummy_stack
            : public layer_payload_encoder<dummy_layer>
        { };

    }
}

TEST(TestLayerPayloadEncoder, api)
{
    kodo::dummy_stack stack;
    stack.m_layer_selected = false;

    stack.m_layer_index_size = rand();
    stack.m_coefficients_size = rand();
    stack.m_payload_size = rand();

    stack.m_encode_symbol_data = 0;
    stack.m_encode_coefficients = 0;

    stack.m_write_layer_index = 0;
    stack.m_write_coefficients = 0;

    uint8_t* payload = (uint8_t*)10;

    uint32_t payload_size = stack.encode(payload);
    EXPECT_EQ(payload_size, stack.m_payload_size);
    EXPECT_TRUE(stack.m_layer_selected);
    EXPECT_EQ(payload, stack.m_write_layer_index);

    // Index written
    EXPECT_EQ(payload + stack.m_layer_index_size,
              stack.m_write_coefficients);

    // Coefficients
    EXPECT_EQ(payload + stack.m_layer_index_size,
              stack.m_write_coefficients);
    EXPECT_EQ(payload + stack.m_layer_index_size,
              stack.m_encode_coefficients);

    // Symbol
    EXPECT_EQ(payload + stack.m_layer_index_size + stack.m_coefficients_size,
              stack.m_encode_symbol_data);

}

