// Copyright Steinwurf ApS 2011-2012.
// Distributed under the "STEINWURF RESEARCH LICENSE 1.0".
// See accompanying file LICENSE.rst or
// http://www.steinwurf.com/licensing

#pragma once

#include <fifi/is_prime2325.hpp>
#include <fifi/prime2325_binary_search.hpp>
#include <fifi/prime2325_apply_prefix.hpp>

#include <kodo/systematic_encoder.hpp>
#include <kodo/layer_setup.hpp>

#include <kodo_unit_test/basic_api_test_helper.hpp>

/// Helper function which will invoke a number of checks on an encoder
/// and decoder pair
template<class Encoder, class Decoder>
inline void helper_layer_api(uint32_t symbol_size, uint32_t l1,
                           uint32_t l2, uint32_t k, double p1)
{
    // Common setting
    typename Encoder::factory encoder_factory(l1 + l2, symbol_size);
    typename Decoder::factory decoder_factory(l1 + l2, symbol_size);

    auto encoder = kodo::default_encoder(
        encoder_factory, symbol_size, l1, l2, k, p1);

    auto decoder = kodo::default_l1l2_decoder(
        decoder_factory, symbol_size, l1, l2, k, k);

    EXPECT_TRUE(l1 +l2 == encoder_factory.max_symbols());
    EXPECT_TRUE(symbol_size == encoder_factory.max_symbol_size());
    EXPECT_TRUE(l1 + l2 == encoder->symbols());
    EXPECT_TRUE(symbol_size == encoder->symbol_size());

    EXPECT_TRUE(l1 + l2 == decoder_factory.max_symbols());
    EXPECT_TRUE(symbol_size == decoder_factory.max_symbol_size());
    EXPECT_TRUE(l1 == decoder->symbols());
    EXPECT_TRUE(symbol_size == decoder->symbol_size());

    EXPECT_TRUE(encoder->symbol_length() > 0);
    EXPECT_TRUE(decoder->symbol_length() > 0);

    EXPECT_TRUE(encoder->block_size() == (l1 + l2) * symbol_size);
    EXPECT_TRUE(decoder->block_size() == l1 * symbol_size);

    EXPECT_TRUE(encoder_factory.max_payload_size() >=
                encoder->payload_size());

    EXPECT_TRUE(decoder_factory.max_payload_size() >=
                decoder->payload_size());

    EXPECT_EQ(encoder_factory.max_payload_size(),
              decoder_factory.max_payload_size());

    // Encode/decode operations
    EXPECT_EQ(encoder->payload_size(), decoder->payload_size());

    std::vector<uint8_t> payload(encoder->payload_size());

    std::vector<uint8_t> data_in = random_vector(encoder->block_size());
    std::vector<uint8_t> data_in_copy(data_in);

    sak::mutable_storage storage_in = sak::storage(data_in);
    sak::mutable_storage storage_in_copy = sak::storage(data_in_copy);

    EXPECT_TRUE(sak::equal(storage_in, storage_in_copy));

    // Only used for prime fields, lets reconsider how we implement
    // this less intrusive
    uint32_t prefix = 0;

    if(fifi::is_prime2325<typename Encoder::field_type>::value)
    {
        // This field only works for multiple of uint32_t
        assert((encoder->block_size() % 4) == 0);

        uint32_t block_length = encoder->block_size() / 4;

        fifi::prime2325_binary_search search(block_length);
        prefix = search.find_prefix(storage_in_copy);

        // Apply the negated prefix
        fifi::apply_prefix(storage_in_copy, ~prefix);
    }

    encoder->set_symbols(storage_in_copy);

    // Set the encoder non-systematic
    if(kodo::is_systematic_encoder(encoder))
        kodo::set_systematic_off(encoder);

    while( !decoder->is_complete() )
    {
        uint32_t payload_used = encoder->encode( &payload[0] );
        EXPECT_TRUE(payload_used <= encoder->payload_size());

        decoder->decode( &payload[0] );
    }

    std::vector<uint8_t> data_out(decoder->block_size(), '\0');
    decoder->copy_symbols(sak::storage(data_out));

    if(fifi::is_prime2325<typename Encoder::field_type>::value)
    {
        // Now we have to apply the negated prefix to the decoded data
        fifi::apply_prefix(sak::storage(data_out), ~prefix);
    }

    // Resize the data_in buffer since we may not have decoded
    // everything only the block size of the coder
    data_in.resize(data_out.size());

    EXPECT_TRUE(data_in == data_out);
}

/// Helper function that invokes the helper_layer_api using a number of
/// different finite fields
template
<
    template <class> class Encoder,
    template <class> class Decoder
>
inline void helper_layer_api(uint32_t symbol_size, uint32_t l1,
                           uint32_t l2, uint32_t k, double p1)
{
    helper_layer_api
        <
            Encoder<fifi::binary>,
            Decoder<fifi::binary>
            >(symbol_size, l1, l2, k, p1);

    helper_layer_api
        <
            Encoder<fifi::binary8>,
            Decoder<fifi::binary8>
            >(symbol_size, l1, l2, k, p1);

    helper_layer_api
        <
            Encoder<fifi::binary16>,
            Decoder<fifi::binary16>
            >(symbol_size, l1, l2, k, p1);

    helper_layer_api
        <
            Encoder<fifi::prime2325>,
            Decoder<fifi::prime2325>
            >(symbol_size, l1, l2, k, p1);
}

/// Helper function that invokes the helper_layer_api functions using a
/// set of symbols and symbol sizes
template
<
    template <class> class Encoder,
    template <class> class Decoder
>
inline void helper_layer_api()
{

    helper_layer_api<Encoder, Decoder>(1600, 10, 6, 2, 0.5);
    helper_layer_api<Encoder, Decoder>(1600, 10, 6, 1, 0.5);
    //helper_layer_api<Encoder, Decoder>(1600, 16, 0, 0, 1.0);
}

