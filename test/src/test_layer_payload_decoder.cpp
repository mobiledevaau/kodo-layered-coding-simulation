#include <gtest/gtest.h>

#include <kodo/layer_payload_decoder.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {
        class layer_dummy_decoder
        {
        public:

            typedef fifi::binary field_type;
            typedef std::shared_ptr<layer_dummy_decoder> pointer;

        public:

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                pointer build()
                {
                    m_decoder = std::make_shared<layer_dummy_decoder>();
                    return m_decoder;
                }

                void set_symbols(uint32_t symbols)
                {
                    m_symbols = symbols;
                }

                void set_symbol_size(uint32_t symbol_size)
                {
                    m_symbol_size = symbol_size;
                }

                void set_elimination_offset(uint32_t offset)
                {
                    m_offset = offset;
                }

            public:

                pointer m_decoder;

                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;

                uint32_t m_symbols;
                uint32_t m_symbol_size;
                uint32_t m_offset;
            };

        public:

            void decode_symbol(uint8_t* symbol_data,
                               uint8_t* symbol_coefficients)
            {
                m_symbol_data = symbol_data;
                m_symbol_coefficents = symbol_coefficients;

                if(m_increase)
                    ++m_rank;
            }

            uint32_t rank()
            {
                return m_rank;
            }

            bool m_increase;

            const uint8_t* m_symbol_data;
            const uint8_t* m_symbol_coefficents;

            uint32_t m_rank;

        };

        class dummy_layer
        {
        public:

            typedef fifi::binary field_type;

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t decode_layer_index() const
                {
                    return m_decode_layer_index;
                }

                uint32_t use_layer_index() const
                {
                    return m_use_layer_index;
                }

                uint32_t layer_symbols(uint32_t index) const
                {
                    return m_layer_symbols[index];
                }

                uint32_t symbol_size() const
                {
                    return m_symbol_size;
                }

                uint32_t symbols() const
                {
                    return m_symbols;
                }

                std::vector<uint32_t> m_layer_symbols;

                uint32_t m_decode_layer_index;
                uint32_t m_use_layer_index;

                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;

                uint32_t m_symbol_size;
                uint32_t m_symbols;
            };

        public:

            template<class Factory>
            void initialize(Factory& the_factory)
            {
                (void) the_factory;
            }

            void read_layer_index(const uint8_t* buffer)
            {
                m_read_layer_index = buffer;
            }

            uint32_t layer_index() const
            {
                return m_layer_index;
            }

            uint32_t use_layer_index() const
            {
                return m_use_layer_index;
            }

            uint32_t decode_layer_index() const
            {
                return m_decode_layer_index;
            }

            uint32_t layer_index_size() const
            {
                return m_layer_index_size;
            }

            uint32_t coefficient_vector_size() const
            {
                return m_coefficients_size;
            }

            void decode_symbol(uint8_t* symbol_data,
                               uint8_t* symbol_coefficients)
            {
                m_symbol_data = symbol_data;
                m_symbol_coefficents = symbol_coefficients;
            }

            const uint8_t* m_read_layer_index;

            uint32_t m_layer_index;
            uint32_t m_use_layer_index;
            uint32_t m_decode_layer_index;
            uint32_t m_layer_index_size;
            uint32_t m_coefficients_size;

            const uint8_t* m_symbol_data;
            const uint8_t* m_symbol_coefficents;

        };

        class dummy_stack
            : public layer_payload_decoder<layer_dummy_decoder, dummy_layer>
        { };

    }
}

TEST(TestLayerPayloadDecoder, api)
{
    uint32_t max_symbols = 10;
    uint32_t max_symbol_size = 100;

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);

    factory.m_decode_layer_index = 0;
    factory.m_use_layer_index = 1;

    factory.m_layer_symbols.push_back(2);
    factory.m_layer_symbols.push_back(4);
    factory.m_layer_symbols.push_back(6);

    // The lines above specified that we will decode the first 2
    // symbols and use the next 2 for the layer decoder. So the
    // symbols in the stack should match 2
    factory.m_symbols = 2;
    factory.m_symbol_size = 50;

    kodo::dummy_stack stack;

    stack.initialize(factory);

    // After initialize the layer factory should be properly
    // initialized
    const auto& layer_factory = factory.layer_factory();

    EXPECT_EQ(layer_factory.m_symbols, 2U);
    EXPECT_EQ(layer_factory.m_symbol_size, 50U);
    EXPECT_EQ(layer_factory.m_offset, 2U);

    // After initialize the layer decoder should also be built
    auto layer_decoder = stack.layer_decoder();
    ASSERT_TRUE((bool)layer_decoder);

    // We emulate a packet coming in going to the layer_decoder
    // the it should be from layer 1
    stack.m_layer_index = 1;
    stack.m_decode_layer_index = 0;
    stack.m_use_layer_index = 1;

    layer_decoder->m_rank = 0;
    layer_decoder->m_increase = true;
    layer_decoder->m_symbol_data = (uint8_t*)0;
    layer_decoder->m_symbol_coefficents = (uint8_t*)0;

    stack.m_symbol_data = (uint8_t*)0;
    stack.m_symbol_coefficents = (uint8_t*)0;

    std::vector<uint8_t> buffer(10);

    stack.decode(&buffer[0]);

    // Since we increased the rank we should only get the packet
    // in the layer decoder
    EXPECT_TRUE(layer_decoder->m_symbol_data != 0);
    EXPECT_TRUE(layer_decoder->m_symbol_coefficents != 0);

    EXPECT_TRUE(stack.m_symbol_data == 0);
    EXPECT_TRUE(stack.m_symbol_coefficents == 0);

    // Now lets not increase the rank then we should see the symbol in
    // the main stack
    layer_decoder->m_increase = false;
    layer_decoder->m_symbol_data = (uint8_t*)0;
    layer_decoder->m_symbol_coefficents = (uint8_t*)0;

    stack.decode(&buffer[0]);

    // Since we increased the rank we should only get the packet
    // in the layer decoder
    EXPECT_TRUE(layer_decoder->m_symbol_data != 0);
    EXPECT_TRUE(layer_decoder->m_symbol_coefficents != 0);

    EXPECT_TRUE(stack.m_symbol_data != 0);
    EXPECT_TRUE(stack.m_symbol_coefficents != 0);

}

