#include <gtest/gtest.h>

#include <kodo/layer_index_selector.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            typedef uint32_t layer_index_type;

        public:

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t symbols() const
                {
                    return m_symbols;
                }

                const std::vector<uint32_t>& layer_symbols() const
                {
                    return m_layer_symbols;
                }

                uint32_t m_symbols;
                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;
                std::vector<uint32_t> m_layer_symbols;

            };

        public:

            template<class Factory>
            void construct(Factory &the_factory)
            {
                (void) the_factory;
            }

            template<class Factory>
            void initialize(Factory &the_factory)
            {
                (void) the_factory;
            }

        };

        class dummy_stack
            : public layer_index_selector<dummy_layer>
        { };

     }
}

TEST(TestLayerIndexSelector, api)
{
    uint32_t max_symbols = 20U;
    uint32_t max_symbol_size = 100U;
    uint32_t symbols = 10U;

    std::vector<double> probabilities;
    probabilities.push_back(0.6);
    probabilities.push_back(0.1);
    probabilities.push_back(0.1);
    probabilities.push_back(0.1);
    probabilities.push_back(0.1);

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);
    factory.m_symbols = symbols;
    factory.m_layer_symbols.resize(5);
    factory.set_layer_probabilities(probabilities);

    kodo::dummy_stack stack;
    stack.construct(factory);
    stack.initialize(factory);

    for(uint32_t i = 0; i < 10; ++i)
    {
        stack.select_layer();
    }

}

