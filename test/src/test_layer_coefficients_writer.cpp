#include <gtest/gtest.h>

#include <kodo/layer_coefficients_writer.hpp>
#include <fifi/field_types.hpp>


namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        template<class Field>
        class dummy_layer
        {
        public:

            typedef Field field_type;
            typedef typename field_type::value_type value_type;

            uint32_t coefficient_vector_size() const
            {
                return fifi::elements_to_size<field_type>(m_symbols);
            }

            uint32_t layer_index() const
            {
                return m_layer_index;
            }

            uint32_t layer_symbols(uint32_t index) const
            {
                EXPECT_EQ(index, m_layer_index);
                return m_layer_symbols;
            }

            uint32_t m_symbols;
            uint32_t m_layer_symbols;
            uint32_t m_layer_index;

        };

        template<class Field>
        class dummy_stack :
            public layer_coefficients_writer<dummy_layer<Field> >
        { };

    }
}

TEST(TestLayerCoefficientsWriter, api)
{
    typedef fifi::binary field_type;
    kodo::dummy_stack<field_type> stack;
    stack.m_symbols = 20;
    stack.m_layer_symbols = 15;
    stack.m_layer_index = 3;

    std::vector<uint8_t> buffer(stack.coefficient_vector_size());
    stack.write_coefficients(&buffer[0]);

    for(uint32_t i = 0; i < stack.m_symbols; ++i)
    {

        // std::cout << (uint32_t) fifi::get_value<field_type>(&buffer[0], i) << " ";
        if(i < stack.m_layer_symbols)
        {
            continue;
        }
        else
        {
            EXPECT_EQ(fifi::get_value<field_type>(&buffer[0], i), 0U);
        }
    }
    // std::cout << std::endl;
}

