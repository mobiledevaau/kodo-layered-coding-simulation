#include <gtest/gtest.h>

#include <kodo/layer_index_writer.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            typedef uint32_t layer_index_type;

            layer_index_type layer_index() const
            {
                return m_layer_index;
            }

            layer_index_type m_layer_index;

        };

        class dummy_stack
            : public layer_index_writer<dummy_layer>
        { };

    }
}

TEST(TestLayerIndexWriter, api)
{
    typedef kodo::dummy_stack::layer_index_type layer_index_type;

    kodo::dummy_stack stack;
    stack.m_layer_index = 10U;

    std::vector<uint8_t> buffer(sizeof(layer_index_type));
    stack.write_layer_index(&buffer[0]);

    auto v = sak::big_endian::get<layer_index_type>(&buffer[0]);
    EXPECT_EQ(v, stack.m_layer_index);

}

