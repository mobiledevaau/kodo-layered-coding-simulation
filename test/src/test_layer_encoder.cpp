#include <gtest/gtest.h>

#include <kodo/layer_decoder.hpp>
#include <kodo/layer_encoder.hpp>

#include "kodo_unit_test/helper_layer_api.hpp"

/// This file runs the kodo api tests on both the layer_decoder and
/// layer_encoder

TEST(TestLayerEncoder, api)
{
    helper_layer_api<kodo::layer_encoder, kodo::layer_decoder>();

}

