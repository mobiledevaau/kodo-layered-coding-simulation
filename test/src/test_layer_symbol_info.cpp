#include <gtest/gtest.h>

#include <kodo/layer_symbol_info.hpp>

namespace kodo
{

    // Put dummy layers and tests classes in an anonymous namespace
    // to avoid violations of ODF (one-definition-rule) in other
    // translation units
    namespace
    {

        class dummy_layer
        {
        public:

            typedef uint32_t layer_index_type;

        public:

            class factory
            {
            public:

                /// @copydoc layer::factory::factory(uint32_t,uint32_t)
                factory(uint32_t max_symbols, uint32_t max_symbol_size)
                    : m_max_symbols(max_symbols),
                      m_max_symbol_size(max_symbol_size)
                { }

                uint32_t symbols() const
                {
                    return m_symbols;
                }

                // This function is needed since the layer will assert
                // that the vector containing the symbol information
                // and the vector containing the layer probabilities
                // are of equal size
                const std::vector<double>& layer_probabilities() const
                {
                    return m_layer_probabilities;
                }

                uint32_t m_symbols;
                uint32_t m_max_symbols;
                uint32_t m_max_symbol_size;
                std::vector<double> m_layer_probabilities;

            };

        public:

            template<class Factory>
            void initialize(Factory &the_factory)
            {
                (void) the_factory;
            }

        };

        class dummy_stack
            : public layer_symbol_info<dummy_layer>
        { };

    }
}

TEST(TestLayerSymbolInfo, api)
{
    uint32_t max_symbols = 20U;
    uint32_t max_symbol_size = 100U;
    uint32_t symbols = 10U;

    std::vector<uint32_t> layer_symbols;
    layer_symbols.push_back(6U);
    layer_symbols.push_back(7U);
    layer_symbols.push_back(8U);
    layer_symbols.push_back(9U);
    layer_symbols.push_back(10U);

    kodo::dummy_stack::factory factory(max_symbols, max_symbol_size);
    factory.m_symbols = symbols;
    factory.m_layer_probabilities.resize(5);
    factory.set_layer_symbols(layer_symbols);

    EXPECT_EQ(factory.layer_symbols(0), layer_symbols[0]);
    EXPECT_EQ(factory.layer_symbols(1), layer_symbols[1]);
    EXPECT_EQ(factory.layer_symbols(2), layer_symbols[2]);
    EXPECT_EQ(factory.layer_symbols(3), layer_symbols[3]);
    EXPECT_EQ(factory.layer_symbols(4), layer_symbols[4]);

    EXPECT_TRUE(factory.layer_symbols() == layer_symbols);

    kodo::dummy_stack stack;
    stack.initialize(factory);

    EXPECT_EQ(stack.layer_symbols(0), layer_symbols[0]);
    EXPECT_EQ(stack.layer_symbols(1), layer_symbols[1]);
    EXPECT_EQ(stack.layer_symbols(2), layer_symbols[2]);
    EXPECT_EQ(stack.layer_symbols(3), layer_symbols[3]);
    EXPECT_EQ(stack.layer_symbols(4), layer_symbols[4]);

    EXPECT_TRUE(stack.layer_symbols() == layer_symbols);
}

