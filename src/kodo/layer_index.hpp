#pragma once

#include <random>

namespace kodo
{

    /// Defines the size of the index needed by both the
    /// layer_index_reader and layer_index_writer
    template<class SuperCoder>
    class layer_index: public SuperCoder
    {
    public:

        /// The data type used to store the index of the layers in the payload
        typedef uint32_t layer_index_type;

    public:

        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size)
                : SuperCoder::factory(max_symbols, max_symbol_size)
            { }

            /// @copydoc layer::factory::max_payload_size() const
            uint32_t max_layer_index_size() const
            {
                return sizeof(layer_index_type);
            }
        };

    public:

        uint32_t layer_index_size() const
        {
            return sizeof(layer_index_type);
        }

    };

}
