#pragma once

#include <cstdint>
#include <kodo/rlnc/full_vector_codes.hpp>

#include "layer_payload_encoder.hpp"
#include "layer_payload.hpp"
#include "layer_index_writer.hpp"
#include "layer_coefficients_writer.hpp"
#include "layer_index_selector.hpp"
#include "layer_symbol_info.hpp"
#include "layer_index.hpp"

namespace kodo
{

    /// Given the size of layer 1, layer 2 and the number k of
    /// sub-layers in layer 2 we calculate the total number of layers
    /// and the size of each layer
    inline std::vector<uint32_t> default_layers(uint32_t l1, uint32_t l2, uint32_t k)
    {
        assert(l1 > 0);
        assert(k <= l2);

        // We have 1 + k layers, where one is l1 and then k layers for l2
        std::vector<uint32_t> layers(k + 1);

        // Set the layer 1
        layers[0] = l1;

        // If we have no l2 layer just stop now
        if(l2 == 0)
            return layers;

        // For the rest we have k layers with increasing l2/k size
        assert((l2 % k) == 0);
        std::fill_n(&layers[1], k, l2/k);

        // Make a cumulative sum
        std::partial_sum(layers.begin(), layers.end(), layers.begin());
        return layers;
    }

    /// Using this function we compute the probability of sending from
    /// each layer, where the first layer has p1 and the remaining k
    /// layers have (1-p1)/k
    inline std::vector<double> default_probabilities(double p1, uint32_t k,
                                              uint32_t layers)
    {
        assert(layers > 0);
        assert(k < layers);
        assert(k + 1 == layers);

        std::vector<double> probability(layers);

        // p1 is the probability for the first layer
        probability[0] = p1;

        // If k == 0 we have special case where we only have one layer
        if(k == 0)
        {
            assert(layers == 1);
            assert(p1 == 1.0);
            return probability;
        }

        // Fill the remaining k slots with the probability (1-p1)/k
        std::fill_n(&probability[1], k, (1-p1)/k);
        return probability;
    }

    template<class Factory>
    inline auto default_encoder(
        Factory& factory, uint32_t symbol_size, uint32_t l1,
        uint32_t l2, uint32_t k, double p1) -> decltype(factory.build())
    {

        factory.set_symbols(l1 + l2);
        factory.set_symbol_size(symbol_size);

        std::vector<uint32_t> layers;
        layers = default_layers(l1, l2, k);

        std::vector<double> probability;
        probability = default_probabilities(p1, k, layers.size());

        factory.set_layer_symbols(layers);
        factory.set_layer_probabilities(probability);

        return factory.build();

    }

    template<class Factory>
    inline auto default_l1l2_decoder(
        Factory& factory, uint32_t symbol_size, uint32_t l1,
        uint32_t l2, uint32_t k, uint32_t k_use) -> decltype(factory.build())
    {

        assert(k_use <= k);

        factory.set_symbols(l1);
        factory.set_symbol_size(symbol_size);

        auto layers = default_layers(l1, l2, k);

        factory.set_layer_symbols(layers);
        factory.set_decode_layer_index(0);

        // If k_use is 1 then since l1 is at index 0 then the first k
        // layer is at index 1 so that should be ok
        factory.set_use_layer_index(k_use);

        return factory.build();

    }

    template<class Factory>
    inline auto default_l2_decoder(
        Factory& factory, uint32_t symbol_size, uint32_t l1,
        uint32_t l2, uint32_t k) -> decltype(factory.build())
    {

        factory.set_symbols(l1 + l2);
        factory.set_symbol_size(symbol_size);

        auto layers = default_layers(l1, l2, k);
        assert(layers.size() > 0);

        factory.set_layer_symbols(layers);
        factory.set_decode_layer_index(layers.size()-1);
        factory.set_use_layer_index(layers.size()-1);

        return factory.build();

    }

    template<class Factory>
    inline auto default_l1_decoder(
        Factory& factory, uint32_t symbol_size, uint32_t l1,
        uint32_t l2, uint32_t k) -> decltype(factory.build())
    {

        factory.set_symbols(l1);
        factory.set_symbol_size(symbol_size);

        auto layers = default_layers(l1, l2, k);
        assert(layers.size() > 0);

        factory.set_layer_symbols(layers);
        factory.set_decode_layer_index(0);
        factory.set_use_layer_index(0);

        return factory.build();

    }



}


