#pragma once

#include <cstdint>
#include <kodo/rlnc/full_vector_codes.hpp>

#include "layer_payload_encoder.hpp"
#include "layer_payload.hpp"
#include "layer_index_writer.hpp"
#include "layer_coefficients_writer.hpp"
#include "layer_index_selector.hpp"
#include "layer_symbol_info.hpp"
#include "layer_index.hpp"

namespace kodo
{

    /// @ingroup fec_stacks
    /// @brief Complete stack implementing a RLNC encoder.
    ///
    template<class Field>
    class layer_encoder :
        public // Payload Codec API
               layer_payload_encoder<
               layer_payload<
               layer_index_writer<
               layer_coefficients_writer<
               layer_index_selector<
               layer_symbol_info<
               layer_index<
               // Codec API
               encode_symbol_tracker<
               zero_symbol_encoder<
               linear_block_encoder<
               storage_aware_encoder<
               // Coefficient Storage API
               coefficient_value_access<
               coefficient_info<
               // Symbol Storage API
               deep_symbol_storage<
               storage_bytes_used<
               storage_block_info<
               // Finite Field API
               finite_field_math<typename fifi::default_field<Field>::type,
               finite_field_info<Field,
               // Factory API
               final_coder_factory_pool<
               // Final type
               layer_encoder<Field
               > > > > > > > > > > > > > > > > > > > >
    { };

}


