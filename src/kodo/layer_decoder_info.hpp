#pragma once

#include <random>
#include <vector>

namespace kodo
{

    /// Stores information about which layers we will decode and which
    /// layers we will use to assist in the decoding.
    template<class SuperCoder>
    class layer_decoder_info : public SuperCoder
    {
    public:

        /// The layer index type
        typedef typename SuperCoder::layer_index_type layer_index_type;

    public:

        /// The factory for this layer
        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size)
                : SuperCoder::factory(max_symbols, max_symbol_size)
            { }

            /// @param layer Set the index of the layers to decode
            void set_decode_layer_index(layer_index_type layer)
            {
                m_decode_layer_index = layer;
            }

            /// @param layer Set the index of the layers to use
            void set_use_layer_index(layer_index_type layer)
            {
                m_use_layer_index = layer;
            }

            /// @return The index of the layers to decode
            layer_index_type decode_layer_index() const
            {
                return m_decode_layer_index;
            }

            /// @return The index of the layers to use
            layer_index_type use_layer_index() const
            {
                return m_use_layer_index;
            }

        protected:

            /// Store the index of the layers to decode
            layer_index_type m_decode_layer_index;

            /// Store the index of the layers to use
            layer_index_type m_use_layer_index;

        };

    public:

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);

            m_decode_layer_index = the_factory.decode_layer_index();
            m_use_layer_index = the_factory.use_layer_index();

            assert(m_decode_layer_index <= m_use_layer_index);
            assert(m_use_layer_index < the_factory.layer_symbols().size());
        }

        /// @return The index of the layers to decode
        layer_index_type decode_layer_index() const
        {
            return m_decode_layer_index;
        }

        /// @return The index of the layers to use
        layer_index_type use_layer_index() const
        {
            return m_use_layer_index;
        }

    protected:

        /// Store the index of the layers to decode
        layer_index_type m_decode_layer_index;

        /// Store the index of the layers to use
        layer_index_type m_use_layer_index;

    };

}

