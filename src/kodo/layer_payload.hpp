#pragma once

#include <random>

namespace kodo
{

    /// Base class specifying the payload of a layer coder
    template<class SuperCoder>
    class layer_payload : public SuperCoder
    {
    public:

        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size)
                : SuperCoder::factory(max_symbols, max_symbol_size)
            { }

            /// @copydoc layer::factory::max_payload_size() const
            uint32_t max_payload_size() const
            {
                return SuperCoder::factory::max_layer_index_size() +
                    SuperCoder::factory::max_coefficient_vector_size() +
                    SuperCoder::factory::max_symbol_size();
            }
        };

    public:

        /// @copydoc layer::payload_size() const
        uint32_t payload_size() const
        {
            return SuperCoder::layer_index_size() +
                SuperCoder::coefficient_vector_size() +
                SuperCoder::symbol_size();
        }
    };

}

