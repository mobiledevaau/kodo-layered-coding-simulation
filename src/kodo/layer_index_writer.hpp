#pragma once

#include <sak/convert_endian.hpp>

namespace kodo
{

    template<class SuperCoder>
    class layer_index_writer: public SuperCoder
    {
    public:

        /// Fetch the type used for the layer index
        typedef typename SuperCoder::layer_index_type layer_index_type;

    public:

        void write_layer_index(uint8_t* buffer) const
        {
            assert(buffer != 0);
            sak::big_endian::put<layer_index_type>(
                SuperCoder::layer_index(), buffer);
        }
    };

}

