#pragma once

#include <random>
#include "layer_payload.hpp"

namespace kodo
{

    template<class SuperCoder>
    class layer_payload_encoder : public SuperCoder
    {
    public:

        uint32_t encode(uint8_t *payload)
        {
            assert(payload != 0);

            SuperCoder::select_layer();

            // Buffer offsets
            uint8_t* index = payload;

            uint8_t* coefficients = index +
                SuperCoder::layer_index_size();

            uint8_t* symbol = coefficients +
                SuperCoder::coefficient_vector_size();

            SuperCoder::write_layer_index(index);
            SuperCoder::write_coefficients(coefficients);
            SuperCoder::encode_symbol(symbol, coefficients);

            return SuperCoder::payload_size();
        }
    };

}

