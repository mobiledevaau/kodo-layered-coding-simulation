#pragma once

#include <random>
#include <fifi/fifi_utils.hpp>

namespace kodo
{

    /// This layer generates the coding coefficients used for the
    /// encoding.
    template<class SuperCoder>
    class layer_coefficients_writer: public SuperCoder
    {
    public:

        /// @copydoc layer::value_type
        typedef typename SuperCoder::field_type field_type;

        /// @copydoc layer::value_type
        typedef typename SuperCoder::value_type value_type;

    public:

        /// Constructor
        layer_coefficients_writer()
            : m_distribution(field_type::min_value, field_type::max_value)
        {
            std::random_device device;
            m_generator = std::mt19937(device());
        }

        void write_coefficients(uint8_t* buffer)
        {
            assert(buffer != 0);

            // Since we will not set all coefficients we should ensure
            // that the non specified ones are zero
            std::fill_n(buffer, SuperCoder::coefficient_vector_size(), 0);

            auto coefficients = reinterpret_cast<value_type*>(buffer);

            auto index = SuperCoder::layer_index();
            auto symbols = SuperCoder::layer_symbols(index);

            for(uint32_t i = 0; i < symbols; ++i)
            {
                auto coefficient = m_distribution(m_generator);

                fifi::set_value<field_type>(coefficients, i, coefficient);
            }
        }

        void seed(uint32_t s)
        {
            m_generator.seed(s);
        }

    protected:

        /// Distribution that generates random values from a finite field
        std::uniform_int_distribution<value_type> m_distribution;

        /// The random generator
        std::mt19937 m_generator;
    };

}

