#pragma once

#include <random>
#include <vector>

namespace kodo
{

    /// Stores information about how many symbols are contained in
    /// each layer.
    /// On the encoder side this information is used to
    template<class SuperCoder>
    class layer_symbol_info : public SuperCoder
    {
    public:

        /// Access the layer index type
        typedef typename SuperCoder::layer_index_type layer_index_type;

    public:

        /// The factory for this layer
        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size)
                : SuperCoder::factory(max_symbols, max_symbol_size)
            { }

            /// @param layer_symbols The symbols stored at each
            /// layer. We expect a cumulative sum e.g. if each layer
            /// has 2 symbols then:
            /// layer 0: 2 symbols
            /// layer 1: 4 symbols
            /// and so forth ...
            void set_layer_symbols(const std::vector<uint32_t> &layer_symbols)
            {
                // Make sure each layer contains a non-zero number of
                // symbols
                for(const auto& v: layer_symbols)
                    assert(v > 0);

                m_layer_symbols = layer_symbols;
            }

            /// @return The description of how many symbols are stored
            ///         in each layer
            const std::vector<uint32_t>& layer_symbols() const
            {
                return m_layer_symbols;
            }

            /// @param index The layer we wish to query
            /// @return the number of symbols in the specific layer
            uint32_t layer_symbols(uint32_t index) const
            {
                assert(index < m_layer_symbols.size());
                return m_layer_symbols[index];
            }

        protected:

            /// Stores the symbols in each layer
            std::vector<uint32_t> m_layer_symbols;

        };

    public:

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);

            m_layer_symbols = the_factory.layer_symbols();
        }

        /// @return The description of how many symbols are stored
        ///         in each layer
        const std::vector<uint32_t>& layer_symbols() const
        {
            return m_layer_symbols;
        }

        /// @param index The layer we wish to query
        /// @return the number of symbols in the specific layer
        uint32_t layer_symbols(uint32_t index) const
        {
            assert(index < m_layer_symbols.size());
            return m_layer_symbols[index];
        }

    protected:

        /// Stores the number of symbols at each layer
        std::vector<uint32_t> m_layer_symbols;

    };

}

