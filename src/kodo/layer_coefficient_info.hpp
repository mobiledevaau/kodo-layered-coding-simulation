#pragma once

#include <random>
#include <vector>

namespace kodo
{

    /// @todo
    template<class SuperCoder>
    class layer_coefficient_info : public SuperCoder
    {
    public:

        typedef typename SuperCoder::field_type field_type;

    public:

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);

            uint32_t encoding_symbols = the_factory.layer_symbols().back();

            m_coefficient_vector_length =
                fifi::elements_to_length<field_type>(encoding_symbols);

            m_coefficient_vector_size =
                fifi::elements_to_size<field_type>(encoding_symbols);

            assert(m_coefficient_vector_length > 0);
            assert(m_coefficient_vector_size > 0);
        }

        /// @copydoc layer::coefficient_vectors() const
        uint32_t coefficient_vectors() const
        {
            return SuperCoder::symbols();
        }

        /// @copydoc layer::coefficient_vector_elements() const
        uint32_t coefficient_vector_elements() const
        {
            return SuperCoder::layer_symbols().back();
        }

        /// @copydoc layer::coefficient_vector_length() const
        uint32_t coefficient_vector_length() const
        {
            assert(m_coefficient_vector_length > 0);
            return m_coefficient_vector_length;
        }

        /// @copydoc layer::coefficient_vector_size() const
        uint32_t coefficient_vector_size() const
        {
            assert(m_coefficient_vector_size > 0);
            return m_coefficient_vector_size;
        }

    private:

        /// The length of coefficients in value_type elements
        uint32_t m_coefficient_vector_length;

        /// The size of coefficients in bytes
        uint32_t m_coefficient_vector_size;

    };

}

