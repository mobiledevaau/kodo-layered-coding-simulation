#pragma once

#include <sak/convert_endian.hpp>

namespace kodo
{

    /// Reads the layer index from a buffer
    template<class SuperCoder>
    class layer_index_reader: public SuperCoder
    {
    public:

        /// Fetch the type used for the layer index
        typedef typename SuperCoder::layer_index_type layer_index_type;

    public:

        /// @param buffer The buffer where we will read the layer
        /// index information
        void read_layer_index(uint8_t* buffer)
        {
            assert(buffer != 0);
            m_layer_index = sak::big_endian::get<layer_index_type>(buffer);
        }

        /// @return The last read layer index
        layer_index_type layer_index() const
        {
            return m_layer_index;
        }

    protected:

        /// Stores the layer index read from the buffer
        layer_index_type m_layer_index;
    };

}

