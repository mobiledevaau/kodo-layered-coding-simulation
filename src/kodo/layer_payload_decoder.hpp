#pragma once

#include <random>
#include <kodo/elimination_decoder.hpp>

#include "layer_payload.hpp"


namespace kodo
{

    /// Splits the payload buffer into the information needed to
    /// correctly decode the incoming symbols.
    ///
    /// Example:
    /// Layer 0: 2 symbols
    /// Layer 1: 4 symbols (adding 2 symbols)
    /// Layer 2: 6 symbols (adding 2 symbols)
    /// Layer 3: 8 symbols (adding 2 symbols)
    ///
    /// Example: If the decode_layer_index is 1 we will try to decode
    /// the first 4 symbols. In addition if the use_layer_index is 2
    /// we will create an elimination decoder for those two symbols
    /// and try to use the to be able to convert packets to layer 1
    template<class LayerDecoder, class SuperCoder>
    class layer_payload_decoder : public SuperCoder
    {
    public:

        /// @copydoc layer::field_type
        typedef LayerDecoder layer_decoder_type;

        typedef typename layer_decoder_type::factory layer_factory_type;
        typedef typename layer_decoder_type::pointer layer_pointer_type;

        /// Check the field finite field
        static_assert(std::is_same<
                          typename layer_decoder_type::field_type,
                          typename SuperCoder::field_type>::value,
                      "The finite fields should match");

    public:

        /// @ingroup factory_layers
        /// The factory layer associated with this coder.
        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size) :
                SuperCoder::factory(max_symbols, max_symbol_size),
                m_layer_factory(max_symbols, max_symbol_size)
            { }

            /// Build the layer decoder
            /// @return a newly built layer decoder
            layer_pointer_type build_layer_decoder()
            {
                // Fetch how many symbols we will decode
                auto decode_layer_index =
                    SuperCoder::factory::decode_layer_index();

                // Fetch how many symbols we will use in the layer decoder
                auto use_layer_index =
                    SuperCoder::factory::use_layer_index();

                assert(decode_layer_index <= use_layer_index);

                auto decode_symbols =
                    SuperCoder::factory::layer_symbols(decode_layer_index);

                auto use_symbols =
                    SuperCoder::factory::layer_symbols(use_layer_index);

                assert(decode_symbols == SuperCoder::factory::symbols());
                assert(decode_symbols <= use_symbols);

                auto layer_symbols = use_symbols - decode_symbols;
                auto symbol_size = SuperCoder::factory::symbol_size();

                assert(layer_symbols > 0);
                assert(symbol_size > 0);

                m_layer_factory.set_symbols(layer_symbols);
                m_layer_factory.set_symbol_size(symbol_size);

                // The number of symbols we actually want to
                // decode. This will be the offset in the layer
                // decoder.
                m_layer_factory.set_elimination_offset(decode_symbols);

                return m_layer_factory.build();
            }

            /// @return True if a layer decoder is needed
            bool need_layer_decoder() const
            {
                return SuperCoder::factory::decode_layer_index() <
                    SuperCoder::factory::use_layer_index();
            }

            const layer_factory_type& layer_factory() const
            {
                return m_layer_factory;
            }


        private:

            /// The stage one factory
            layer_factory_type m_layer_factory;

        };

    public:

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);

            m_layer_decoder.reset();

            if(the_factory.need_layer_decoder())
                m_layer_decoder = the_factory.build_layer_decoder();
        }

        /// @param payload The buffer containing the encoded symbol
        ///        data and info
        void decode(uint8_t *payload)
        {
            assert(payload);

            // Buffer offsets
            uint8_t* index = payload;

            uint8_t* coefficients =
                index + SuperCoder::layer_index_size();

            uint8_t* symbol =
                coefficients + SuperCoder::coefficient_vector_size();

            SuperCoder::read_layer_index(index);
            auto layer = SuperCoder::layer_index();

            // If the layer index is higher that what we use we don't
            // even look at it
            if(layer > SuperCoder::use_layer_index())
                return;

            // If we reach here the index is either one that we use or
            // decode
            if(layer > SuperCoder::decode_layer_index())
            {
                if(need_layer_decoder())
                {
                    assert(m_layer_decoder);

                    uint32_t old_rank = m_layer_decoder->rank();
                    m_layer_decoder->decode_symbol(symbol, coefficients);

                    uint32_t new_rank = m_layer_decoder->rank();
                    assert(old_rank <= new_rank);

                    // If the rank was not increased the symbol did either
                    // not have any non-zero coeffficients in the
                    // extension coefficients or it was reduced to zero in
                    // the layer decoder
                    if(old_rank < new_rank)
                        return;
                }
            }

            SuperCoder::decode_symbol(symbol, coefficients);
        }

        /// @return The decoder for the used layer
        const layer_pointer_type& layer_decoder() const
        {
            return m_layer_decoder;
        }

        bool need_layer_decoder() const
        {
            return SuperCoder::decode_layer_index() <
                SuperCoder::use_layer_index();
        }


    protected:

        /// The layer decoder
        layer_pointer_type m_layer_decoder;

    };

}

