#pragma once

#include <cstdint>
#include <kodo/rlnc/full_vector_codes.hpp>
#include <kodo/elimination_decoder.hpp>
#include <kodo/copy_payload_decoder.hpp>

#include "layer_payload_decoder.hpp"
#include "layer_payload.hpp"
#include "layer_index_reader.hpp"
#include "layer_index_selector.hpp"
#include "layer_symbol_info.hpp"
#include "layer_index.hpp"
#include "layer_decoder_info.hpp"
#include "layer_coefficient_info.hpp"

namespace kodo
{

    /// @ingroup fec_stacks
    /// @brief Implementation of a complete RLNC decoder
    ///
    /// This configuration adds the following features (including those
    /// described for the encoder):
    /// - Recoding using the recoding_stack
    /// - Linear block decoder using Gauss-Jordan elimination.
    template<class Field>
    class layer_decoder
        : public // Payload API
                 copy_payload_decoder<
                 layer_payload_decoder<elimination_decoder<Field>,
                 layer_payload<
                 layer_coefficient_info<
                 layer_index_reader<
                 layer_symbol_info<
                 layer_decoder_info<
                 layer_index<
                 // Decoder API
                 aligned_coefficients_decoder<
                 forward_linear_block_decoder<
                 symbol_decoding_status_counter<
                 symbol_decoding_status_tracker<
                 // Coefficient Storage API
                 coefficient_value_access<
                 coefficient_storage<
                 coefficient_info<
                 // Storage API
                 deep_symbol_storage<
                 storage_bytes_used<
                 storage_block_info<
                 // Finite Field API
                 finite_field_math<typename fifi::default_field<Field>::type,
                 finite_field_info<Field,
                 // Factory API
                 final_coder_factory_pool<
                 // Final type
                 layer_decoder<Field>
                     > > > > > > > > > > > > > > > > > > > > >
    { };

}


