#pragma once

#include <random>
#include <vector>

namespace kodo
{

    /// Provides functionality needed to choose which layer to send
    /// from next.
    template<class SuperCoder>
    class layer_index_selector : public SuperCoder
    {
    public:

        /// Access the layer index type
        typedef typename SuperCoder::layer_index_type layer_index_type;

    public:

        /// Factory for this layer
        class factory : public SuperCoder::factory
        {
        public:

            /// @copydoc layer::factory::factory(uint32_t,uint32_t)
            factory(uint32_t max_symbols, uint32_t max_symbol_size)
                : SuperCoder::factory(max_symbols, max_symbol_size)
            { }

            /// @param probabilities Specify the probability associated
            /// with choosing each layer.
            void set_layer_probabilities(
                const std::vector<double>& probabilities)
            {
                assert(probabilities.size() > 0);

                // Make sure each layer has a non-zero probability
                for(const auto& p: probabilities)
                    assert(p > 0.0);

                m_probabilities = probabilities;
            }

            /// @return The specified layer probabilities
            const std::vector<double>& layer_probabilities() const
            {
                assert(m_probabilities.size() > 0);
                return m_probabilities;
            }

        protected:

            /// Stores the probability associated with each layer
            std::vector<double> m_probabilities;

        };

    public:

        /// @copydoc layer::construct(Factory&)
        template<class Factory>
        void construct(Factory& the_factory)
        {
            SuperCoder::construct(the_factory);

            // Initialize the random generator
            std::random_device random_device;
            m_generator = std::mt19937(random_device());
        }

        /// @copydoc layer::initialize(Factory&)
        template<class Factory>
        void initialize(Factory& the_factory)
        {
            SuperCoder::initialize(the_factory);

            std::vector<double> layer_probabilities =
                the_factory.layer_probabilities();

            // Make sure that the number of layers match
            assert(the_factory.layer_symbols().size() ==
                   layer_probabilities.size());

            m_distribution = std::discrete_distribution<layer_index_type>(
                layer_probabilities.begin(), layer_probabilities.end());
        }

        /// Selects a layer index based on the specified probability
        /// distribution
        void select_layer()
        {
            m_selected_layer = m_distribution(m_generator);
        }

        /// @return The previously selected layer
        layer_index_type layer_index() const
        {
            return m_selected_layer;
        }

    protected:

        /// Stores the selected layer
        layer_index_type m_selected_layer;

        /// Random generator used by the distribution
        std::mt19937 m_generator;

        /// Probability distribution used
        std::discrete_distribution<layer_index_type> m_distribution;

    };

}

