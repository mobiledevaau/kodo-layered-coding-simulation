#include <ctime>
#include <string>

#include <kodo/rlnc/full_vector_codes.hpp>
#include "kodo/layer_encoder.hpp"
#include "kodo/layer_decoder.hpp"
#include "kodo/layer_setup.hpp"
#include <gauge/gauge.hpp>

// The simulator uses the gauge benchmarking tool for
// driving and providing options to the simulations.
template<class Encoder, class Decoder>
class coding_simulation : public gauge::benchmark
{
public:

    typedef typename Encoder::factory encoder_factory;
    typedef typename Encoder::pointer encoder_ptr;

    typedef typename Decoder::factory decoder_factory;
    typedef typename Decoder::pointer decoder_ptr;

    /// Start benchmarking (not needed for the simulator)
    void start()
    { }

    /// Stop benchmarking (not needed for the simulator)
    void stop()
    { }

    /// Store the results from a run in the simulator
    void store_run(tables::table& results)
    {
        if(!results.has_column("used"))
            results.add_column("used");

        results.set_value("used", m_packets_used);
    }

    /// Build the topology and setup the simulation
    void setup()
    {
        gauge::config_set cs = get_current_configuration();

        uint32_t symbols = cs.get_value<uint32_t>("symbols");
        uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");
        bool systematic = cs.get_value<bool>("systematic");

        m_decoder_factory = std::make_shared<decoder_factory>(
            symbols, symbol_size);

        m_encoder_factory = std::make_shared<encoder_factory>(
            symbols, symbol_size);

        m_decoder_factory->set_symbols(symbols);
        m_decoder_factory->set_symbol_size(symbol_size);

        m_encoder_factory->set_symbols(symbols);
        m_encoder_factory->set_symbol_size(symbol_size);

        m_encoder = m_encoder_factory->build();
        m_decoder = m_decoder_factory->build();

        // We switch any systematic operations off so we code
        // symbols from the beginning
        if(kodo::is_systematic_encoder(m_encoder))
        {
            if(systematic)
            {
                kodo::set_systematic_on(m_encoder);
            }
            else
            {
                kodo::set_systematic_off(m_encoder);
            }
        }

        // Prepare the data to be encoded
        m_encoded_data.resize(m_encoder->block_size());
        std::generate_n(m_encoded_data.begin(), m_encoded_data.size(), rand);

        m_encoder->set_symbols(sak::storage(m_encoded_data));

        m_packets_used = 0;
    }

    /// Run the simulation
    void run_simulation()
    {
        assert(m_packets_used == 0);
        assert(m_encoder);
        assert(m_decoder);

        std::vector<uint8_t> payload(m_encoder->payload_size());

        // Ensure the encoding vectors generated are randomized between
        // runs
        m_encoder->seed(rand());

        // The clock is running
        RUN{

            while(!m_decoder->is_complete())
            {
                m_encoder->encode(&payload[0]);

                ++m_packets_used;
                m_decoder->decode(&payload[0]);
            }
        }
    }

    /// The unit we are measuring in
    std::string unit_text() const
    {
        return "packets";
    }

    /// Get the options specified on the command-line and map this to a
    /// gauge::config which will be used by the simulator to run different
    /// configurations.
    void get_options(gauge::po::variables_map& options)
    {
        auto symbols =
            options["symbols"].as<uint32_t>();

        auto symbol_size =
            options["symbol_size"].as<uint32_t>();

        auto systematic =
            options["systematic"].as<bool>();


        gauge::config_set cs;
        cs.set_value<uint32_t>("symbols", symbols);
        cs.set_value<uint32_t>("symbol_size", symbol_size);
        cs.set_value<bool>("systematic", systematic);

        add_configuration(cs);

    }

protected:

    /// Count the number of packets used
    uint32_t m_packets_used;

    /// The decoder factory
    std::shared_ptr<decoder_factory> m_decoder_factory;

    /// The encoder factory
    std::shared_ptr<encoder_factory> m_encoder_factory;

    /// The encoder to use
    encoder_ptr m_encoder;

    /// The encoder to use
    decoder_ptr m_decoder;

    /// The data to encode
    std::vector<uint8_t> m_encoded_data;

};

template<class Encoder, class Decoder>
class layered_coding_simulation :
    public coding_simulation<Encoder, Decoder>
{
public:

    typedef typename Encoder::factory encoder_factory;
    typedef typename Encoder::pointer encoder_ptr;

    typedef typename Decoder::factory decoder_factory;
    typedef typename Decoder::pointer decoder_ptr;

    typedef coding_simulation<Encoder, Decoder> base;

    using base::m_decoder_factory;
    using base::m_encoder_factory;
    using base::m_decoder;
    using base::m_encoder;
    using base::m_packets_used;
    using base::m_encoded_data;

    void setup()
    {
        gauge::config_set cs = base::get_current_configuration();

        uint32_t l1 = cs.get_value<uint32_t>("l1");
        uint32_t l2 = cs.get_value<uint32_t>("l2");
        uint32_t k = cs.get_value<uint32_t>("k");
        double p1 = cs.get_value<double>("p1");
        uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");

        m_encoder_factory = std::make_shared<encoder_factory>(
            l1+l2, symbol_size);

        m_decoder_factory = std::make_shared<decoder_factory>(
            l1+l2, symbol_size);

        m_encoder = default_encoder(*m_encoder_factory, symbol_size,
                                    l1, l2, k, p1);

        m_decoder = default_l1l2_decoder(*m_decoder_factory, symbol_size,
                                         l1, l2, k, k);

        // Prepare the data to be encoded
        m_encoded_data.resize(m_encoder->block_size());
        std::generate_n(m_encoded_data.begin(), m_encoded_data.size(), rand);

        m_encoder->set_symbols(sak::storage(m_encoded_data));

        m_packets_used = 0;
    }

    /// Get the options specified on the command-line and map this to a
    /// gauge::config which will be used by the simulator to run different
    /// configurations.
    void get_options(gauge::po::variables_map& options)
    {
        auto l1 = options["l1"].as<uint32_t>();
        auto l2 = options["l2"].as<uint32_t>();
        auto k = options["k"].as<uint32_t>();
        auto p1 = options["p1"].as<double>();
        auto symbol_size = options["symbol_size"].as<uint32_t>();

        gauge::config_set cs;
        cs.set_value<uint32_t>("l1", l1);
        cs.set_value<uint32_t>("l2", l2);
        cs.set_value<uint32_t>("k", k);
        cs.set_value<double>("p1", p1);
        cs.set_value<uint32_t>("symbol_size", symbol_size);

        base::add_configuration(cs);

    }

};

BENCHMARK_OPTION(coding)
{
    gauge::po::options_description options("Coding");

    options.add_options()
        ("symbols",
         gauge::po::value<uint32_t>()->default_value(32),
         "Set symbols");

    options.add_options()
        ("symbol_size",
         gauge::po::value<uint32_t>()->default_value(1400),
         "Set symbols size");

    options.add_options()
        ("systematic",
         gauge::po::value<bool>()->default_value(false),
         "Set the encoder systematic");

    gauge::runner::instance().register_options(options);
}


BENCHMARK_OPTION(coding_layers)
{
    gauge::po::options_description options("Coding");

    options.add_options()
        ("p1",
         gauge::po::value<double>()->default_value(0.5),
         "Set probability for L1");

    options.add_options()
        ("l1",
         gauge::po::value<uint32_t>()->default_value(16),
         "Set symbols for L1");

    options.add_options()
        ("l2",
         gauge::po::value<uint32_t>()->default_value(16),
         "Set symbols for L2");

    options.add_options()
        ("k",
         gauge::po::value<uint32_t>()->default_value(4),
         "Set symbols for k");

    options.add_options()
        ("symbol_size",
         gauge::po::value<uint32_t>()->default_value(1400),
         "Set symbols size");

    gauge::runner::instance().register_options(options);
}

typedef coding_simulation<
    kodo::full_rlnc_encoder<fifi::binary>,
    kodo::full_rlnc_decoder<fifi::binary> > stdfixture;

BENCHMARK_F(stdfixture, Standard, binary, 10)
{
    run_simulation();
}

typedef layered_coding_simulation<
    kodo::layer_encoder<fifi::binary>,
    kodo::layer_decoder<fifi::binary> > layerfixture;

BENCHMARK_F(layerfixture, Layer, binary, 10)
{
    run_simulation();
}



int main(int argc, const char *argv[])
{
    srand(static_cast<uint32_t>(time(0)));

    gauge::runner::add_default_printers();
    gauge::runner::run_benchmarks(argc, argv);

    return 0;
}
