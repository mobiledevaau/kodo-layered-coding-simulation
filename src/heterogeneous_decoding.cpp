#include <ctime>
#include <string>

#include <kodo/rlnc/full_vector_codes.hpp>
#include "kodo/layer_encoder.hpp"
#include "kodo/layer_decoder.hpp"
#include "kodo/layer_setup.hpp"
#include <gauge/gauge.hpp>

template<class Field>
class heterogeneous_decoding : public gauge::benchmark
{
public:

    typedef typename kodo::layer_encoder<Field>::factory encoder_factory;
    typedef typename kodo::layer_encoder<Field>::pointer encoder_pointer;

    typedef typename kodo::layer_decoder<Field>::factory decoder_l1_factory;
    typedef typename kodo::layer_decoder<Field>::pointer decoder_l1_pointer;

    typedef typename kodo::layer_decoder<Field>::factory decoder_l1l2_factory;
    typedef typename kodo::layer_decoder<Field>::pointer decoder_l1l2_pointer;

    typedef typename kodo::layer_decoder<Field>::factory decoder_l2_factory;
    typedef typename kodo::layer_decoder<Field>::pointer decoder_l2_pointer;


    /// Start benchmarking (not needed for the simulator)
    void start()
    { }

    /// Stop benchmarking (not needed for the simulator)
    void stop()
    { }

    /// Store the results from a run in the simulator
    void store_run(tables::table& results)
    {
        if(!results.has_column("l1_used"))
            results.add_column("l1_used");

        if(!results.has_column("l2_used"))
            results.add_column("l2_used");

        if(!results.has_column("l1l2_used"))
            results.add_column("l1l2_used");

        if(!results.has_column("total_tx"))
            results.add_column("total_tx");


        results.set_value("l1_used", m_l1_used);
        results.set_value("l2_used", m_l2_used);
        results.set_value("l1l2_used", m_l1l2_used);
        results.set_value("total_tx", m_total_tx);
    }

    void setup()
    {
        gauge::config_set cs = get_current_configuration();

        uint32_t l1 = cs.get_value<uint32_t>("l1");
        uint32_t l2 = cs.get_value<uint32_t>("l2");
        uint32_t k = cs.get_value<uint32_t>("k");
        uint32_t k_use = cs.get_value<uint32_t>("k_use");
        double p1 = cs.get_value<double>("p1");
        uint32_t symbol_size = cs.get_value<uint32_t>("symbol_size");
        e_l1_used = cs.get_value<double>("e_l1");
        e_l2_used = cs.get_value<double>("e_l2");
        e_l1l2_used = cs.get_value<double>("e_l1l2");

        m_encoder_factory = std::make_shared<encoder_factory>(
            l1+l2, symbol_size);

        m_decoder_l1_factory = std::make_shared<decoder_l1_factory>(
            l1+l2, symbol_size);

        m_decoder_l2_factory = std::make_shared<decoder_l2_factory>(
            l1+l2, symbol_size);

        m_decoder_l1l2_factory = std::make_shared<decoder_l1l2_factory>(
            l1+l2, symbol_size);

        m_encoder = default_encoder(*m_encoder_factory, symbol_size,
                                    l1, l2, k, p1);

        m_l1_decoder = default_l1_decoder(*m_decoder_l1_factory, symbol_size,
                                          l1, l2, k);

        m_l2_decoder = default_l2_decoder(*m_decoder_l2_factory, symbol_size,
                                          l1, l2, k);

        m_l1l2_decoder = default_l1l2_decoder(*m_decoder_l1l2_factory, symbol_size,
                                              l1, l2, k, k_use);


        // Prepare the data to be encoded
        m_encoded_data.resize(m_encoder->block_size());
        std::generate_n(m_encoded_data.begin(), m_encoded_data.size(), rand);

        m_encoder->set_symbols(sak::storage(m_encoded_data));

        m_l1_used = 0;
        m_l2_used = 0;
        m_l1l2_used = 0;
	m_total_tx = 0;
    }

    /// Get the options specified on the command-line and map this to a
    /// gauge::config which will be used by the simulator to run different
    /// configurations.
    void get_options(gauge::po::variables_map& options)
    {
        auto l1 = options["l1"].as<uint32_t>();
        auto l2 = options["l2"].as<uint32_t>();
        auto k = options["k"].as<uint32_t>();
        auto k_use = options["k_use"].as<uint32_t>();
        auto p1 = options["p1"].as<double>();
        auto symbol_size = options["symbol_size"].as<uint32_t>();

        auto e_l1 = options["e_l1"].as<std::vector<double> >();
        auto e_l2 = options["e_l2"].as<std::vector<double> >();
        auto e_l1l2 = options["e_l1l2"].as<std::vector<double> >();

        assert(e_l1.size() > 0);
        assert(e_l2.size() > 0);
        assert(e_l1l2.size() > 0);


        for(uint32_t i = 0; i < e_l1.size(); ++i )
        {
	    for(uint32_t j = 0; j < e_l2.size(); ++j )
	    {
		for(uint32_t u = 0; u < e_l1l2.size(); ++u)
		{
		    gauge::config_set cs;
		    cs.set_value<uint32_t>("l1", l1);
		    cs.set_value<uint32_t>("l2", l2);
		    cs.set_value<uint32_t>("k", k);
                    cs.set_value<uint32_t>("k_use", k_use);
		    cs.set_value<double>("p1", p1);
		    cs.set_value<uint32_t>("symbol_size", symbol_size);
		    cs.set_value<double>("e_l1", e_l1[i]);
		    cs.set_value<double>("e_l2", e_l2[j]);
		    cs.set_value<double>("e_l1l2", e_l1l2[u]);
		    add_configuration(cs);
		}
	    }
        }
    }


    /// Run the simulation
    void run_simulation()
    {
        assert(m_l1_used == 0);
        assert(m_l2_used == 0);
        assert(m_l1l2_used == 0);
        assert(m_encoder);
        assert(m_l1_decoder);
        assert(m_l2_decoder);
        assert(m_l1l2_decoder);
        assert(e_l1_used>=0);
        assert(e_l2_used>=0);
        assert(e_l1l2_used>=0);
        assert(e_l1_used<1);
        assert(e_l2_used<1);
        assert(e_l1l2_used<1);
        assert(m_total_tx==0);

        std::vector<uint8_t> payload(m_encoder->payload_size());

        // Ensure the encoding vectors generated are randomized between
        // runs
        m_encoder->seed(rand());

        // The clock is running
        RUN{

            while(!m_l1_decoder->is_complete() ||
                  !m_l2_decoder->is_complete() ||
                  !m_l1l2_decoder->is_complete())
            {
                m_encoder->encode(&payload[0]);
		++m_total_tx;

                if(!m_l1_decoder->is_complete() && std::rand()> (e_l1_used* RAND_MAX))
                {
                    ++m_l1_used;
                    m_l1_decoder->decode(&payload[0]);
                }

                if(!m_l2_decoder->is_complete() && std::rand()> (e_l2_used* RAND_MAX))
                {
                    ++m_l2_used;
                    m_l2_decoder->decode(&payload[0]);
                }

                if(!m_l1l2_decoder->is_complete() && std::rand()> (e_l1l2_used* RAND_MAX))
                {
                    ++m_l1l2_used;
                    m_l1l2_decoder->decode(&payload[0]);
                }

            }
        }
    }

    /// The unit we are measuring in
    std::string unit_text() const
    {
        return "packets";
    }



protected:

    /// Count the number of packets used
    uint32_t m_l1_used;
    uint32_t m_l2_used;
    uint32_t m_l1l2_used;
    uint32_t m_total_tx;

    /// The decoder factory
    std::shared_ptr<decoder_l1_factory> m_decoder_l1_factory;
    std::shared_ptr<decoder_l2_factory> m_decoder_l2_factory;
    std::shared_ptr<decoder_l1l2_factory> m_decoder_l1l2_factory;

    /// The encoder factory
    std::shared_ptr<encoder_factory> m_encoder_factory;

    /// The encoder to use
    encoder_pointer m_encoder;

    /// The encoder to use
    decoder_l1_pointer m_l1_decoder;
    decoder_l2_pointer m_l2_decoder;
    decoder_l1l2_pointer m_l1l2_decoder;

    /// The data to encode
    std::vector<uint8_t> m_encoded_data;

    /// The loss probability associated to each channel
    double e_l1_used;
    double e_l2_used;
    double e_l1l2_used;
};


BENCHMARK_OPTION(coding_layers)
{
    gauge::po::options_description options("Coding");

    options.add_options()
        ("symbols",
         gauge::po::value<uint32_t>()->default_value(32),
         "Set symbols");

    options.add_options()
        ("symbol_size",
         gauge::po::value<uint32_t>()->default_value(1400),
         "Set symbols size");

    options.add_options()
        ("p1",
         gauge::po::value<double>()->default_value(0.5),
         "Set probability for L1");

    options.add_options()
        ("l1",
         gauge::po::value<uint32_t>()->default_value(16),
         "Set symbols for L1");

    options.add_options()
        ("l2",
         gauge::po::value<uint32_t>()->default_value(16),
         "Set symbols for L2");

    options.add_options()
        ("k",
         gauge::po::value<uint32_t>()->default_value(4),
         "Set symbols for k");

    options.add_options()
        ("k_use",
         gauge::po::value<uint32_t>()->default_value(2),
         "Set the number of k sub-layers to use");

    options.add_options()
        ("symbol_size",
         gauge::po::value<uint32_t>()->default_value(1400),
         "Set symbols size");


    std::vector<double> e_l1;
    e_l1.push_back(0.0);

    auto default_e_l1 =
    gauge::po::value<std::vector<double> >()->default_value(
            e_l1, "")->multitoken();

    options.add_options()
        ("e_l1", default_e_l1, "Set loss probability of L1 Receiver");

    std::vector<double> e_l2;
    e_l2.push_back(0.0);

    auto default_e_l2 =
    gauge::po::value<std::vector<double> >()->default_value(
            e_l2, "")->multitoken();

    options.add_options()
        ("e_l2", default_e_l2, "Set loss probability of L2 Receiver");

    std::vector<double> e_l1l2;
    e_l1l2.push_back(0.0);
    e_l1l2.push_back(0.1);
    e_l1l2.push_back(0.2);
    e_l1l2.push_back(0.3);
    e_l1l2.push_back(0.4);
    e_l1l2.push_back(0.5);
    e_l1l2.push_back(0.6);
    e_l1l2.push_back(0.7);
    e_l1l2.push_back(0.8);
    e_l1l2.push_back(0.9);

    auto default_e_l1l2 =
    gauge::po::value<std::vector<double> >()->default_value(
            e_l1l2, "")->multitoken();

    options.add_options()
        ("e_l1l2", default_e_l1l2, "Set loss probability of L1L2 Receiver");


    gauge::runner::instance().register_options(options);
}

typedef heterogeneous_decoding<fifi::binary> heterogeneous_decoding_binary;

BENCHMARK_F(heterogeneous_decoding_binary, Heterogeneous, binary, 10)
{
    run_simulation();
}


int main(int argc, const char *argv[])
{
    srand(static_cast<uint32_t>(time(0)));

    gauge::runner::add_default_printers();
    gauge::runner::run_benchmarks(argc, argv);

    return 0;
}
